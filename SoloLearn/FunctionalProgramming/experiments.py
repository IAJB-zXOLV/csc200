#Recursion|Self-Reference
def fact(x):
    if x == 1:
        return 1
    else:
        return x * fact(x-1)
print(fact(4))
#this would return 24
def even(x):
    if x == 0:
        return True
    else:
        return odd(x-1)

def odd(x):
    return not even(x)
print(odd(17))
print(even(23))
#returns True & False respectively
def fib(x):
  if x == 0 or x == 1:
    return 1
  else:
    return fib(x-1) + fib(x-2)
print(fib(4))

#sets|counts with like list functions
the_set = {1,2,3,4,5}
a_set = set(["spam","eggs","sausage"])
print(3 in the_set)
print("spam" not in a_set)
#you instead of append use add and you can also use remove (sets can't have duplicates
nums = {1, 2, 1, 3, 1, 4, 5, 6}
print(nums)
nums.add(-7)
nums.remove(3)
print(nums)
#as a list you can use len
#also there is the use of |, &, -, ^
first = {1, 2, 3, 4, 5, 6}
second = {4, 5, 6, 7, 8, 9}

print(first | second)
print(first & second)
print(first - second)
print(second - first)
print(first ^ second)
#itertools is a library that uses count, cycle, repeat, takewhile, chain, accumulate, product, permutations
from itertools import count

for i in count(3):
  print(i)
  if i >=11:
    break
#takewhile I don't understand it|
#Experimenting
def retro(x):
    if x == 0:
        return 0
    if x == 1:
        return 1
    else: 
        return x ** retro(x//2)
print(retro(4))
##
def prime(x):
    if x == 1 or x == 2 or x == 3 or x == 5 or x == 7:
        return True
    if x == 0:
        return "Is Zero!"
    else:
        return no_prime(x)
def no_prime(x):
    if x % 2 == 0:
        return False
    elif x % 3 == 0:
        return False
    elif x % 5 == 0:
        return False
    elif x % 7 == 0:
        return False
    else:
        return prime(x-1)
print(prime(199)) #it doesn't work, problems in the code to be solved
print(prime(12))
##
def count(x):
    if x == 0:
        return
    else:
        print(x)
        count(x-1)
count(7)
##
alo = {2,4,6,8,10}
sett = {1,2,3,4,5,6,7,8,9,10}
for i in sett:
    print(i)
    if i in alo:
        print(i in alo)
    else:
        print(i in alo)
