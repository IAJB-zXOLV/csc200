#classes
class Snake:
    snake = "Is a snake"
    def __init__(self, specie, size):
        self.specie = specie
        self.size = size
    def hiss(self):
        print("hisssssss")

Robert = Snake("Python", "6 feet")
Samantha = Snake("Black Mamba", "4 feet")
print(Samantha.specie) 
print(Samantha.size)
print(Samantha.snake)
Samantha.hiss()
#inheritance
class Classes:
    def __init__(self, name, room):
        self.name = name
        self.room = room
class Science(Classes):
    def Stuff(self):
        print("Science stuff")
class Math(Classes):
    def Mtuff(self):
        print("Math stuff")
Tcowan = Math("Teacher Cowan"," 553")
print(Tcowan.name)
Tcowan.Mtuff()
#multiple inheritance
class A:
    def m(self):
        print("A")
class B(A):
    def n(self):
        print("B")
class C(B):
    def nm(self):
        print("C")
        super().n()

c = C()
c.m()
c.n()
c.nm()
C().n()
#Magic Methods
class Vector2D:
  def __init__(self, x, y):
    self.x = x
    self.y = y
  def __add__(self, other):
    return Vector2D(self.x + other.x, self.y + other.y)

first = Vector2D(5, 7)
second = Vector2D(3, 9)
result = first + second
print(result.x)
print(result.y)


class SpecialString:
  def __init__(self, cont):
    self.cont = cont

  def __gt__(self, other):
    for index in range(len(other.cont)+1):
      result = other.cont[:index] + ">" + self.cont
      result += ">" + other.cont[index:]
      print(result)

spam = SpecialString("spam")
eggs = SpecialString("eggs")
spam > eggs

class glob:
    def __init__(self, name, age, frecuence):
        self.name = name
        self.age = age
        self.frecuence = frecuence

class volc(glob):
    def erp(self):
        print("CEEE")
ff = volc("Cracatoa", "near 10 million years", "2 centuries")
ff.erp()
print(ff.name)
print(ff.age)
print(ff.frecuence)
#Object lifetime
a = 243 #create object
b = a
c = [a]
del a #destroy object
b = 212112
c[0] = -1 
#Data hiding
class Queue:
  def __init__(self, contents):
    self._hiddenlist = list(contents)

  def push(self, value):
    self._hiddenlist.insert(0, value)
   
  def pop(self):
    return self._hiddenlist.pop(-1)

  def __repr__(self):
    return "Queue({})".format(self._hiddenlist)

queue = Queue([1, 2, 3])
print(queue)
queue.push(0)
print(queue)
queue.pop()
print(queue)
print(queue._hiddenlist)
###
class Spam:
  __egg = 7
  def print_egg(self):
    print(self.__egg)

s = Spam()
s.print_egg()
print(s._Spam__egg)
## Class Methods
class Rectangle:
  def __init__(self, width, height):
    self.width = width
    self.height = height

  def calculate_area(self):
    return self.width * self.height

  @classmethod
  def new_square(cls, side_length):
    return cls(side_length, side_length)

square = Rectangle.new_square(5)
print(square.calculate_area())
## Static Methods
class Pizza:
  def __init__(self, toppings):
    self.toppings = toppings

  @staticmethod
  def validate_topping(topping):
    if topping == "pineapple":
      raise ValueError("No pineapples!")
    else:
      return True

ingredients = ["cheese", "onions", "spam"]
if all(Pizza.validate_topping(i) for i in ingredients):
  pizza = Pizza(ingredients)
print(pizza.toppings)
## Properties
class Pizza:
  def __init__(self, toppings):
    self.toppings = toppings
    self._pineapple_allowed = False

  @property
  def pineapple_allowed(self):
    return self._pineapple_allowed

  @pineapple_allowed.setter
  def pineapple_allowed(self, value):
    if value:
      password = input("Enter the password: ")
      if password == "Sw0rdf1sh!":
        self._pineapple_allowed = value
      else:
        raise ValueError("Alert! Intruder!")

pizza = Pizza(["cheese", "tomato"])
print(pizza.pineapple_allowed)
pizza.pineapple_allowed = True
print(pizza.pineapple_allowed)
