from gasp import *

GRID_SIZE = 30
MARGIN = GRID_SIZE

BACKGROUND_COLOR = color.BLACK
WALL_COLOR = (0.6 * 255, 0.9 * 255, 0.9 * 255)

CHOMP_COLOR = color.YELLOW
CHOMP_SIZE = GRID_SIZE * 0.8
CHOMP_SPEED = 0.25           

FOOD_COLOR = color.RED
FOOD_SIZE = GRID_SIZE * 0.15   

GHOST_COLORS = [color.RED,            
                color.GREEN,          
                color.BLUE,   
                color.PURPLE]

GHOST_SPEED = 0.25        

GHOST_SHAPE = [
    (0, -0.5),
    (0.25, -0.75),
    (0.5, -0.5),  
    (0.75, -0.75),
    (0.75, 0.5),
    (0.5, 0.75),
    (-0.5, 0.75),
    (-0.75, 0.5),
    (-0.75, -0.75),
    (-0.5, -0.5),
    (-0.25, -0.75)
  ]

CAPSULE_COLOR = color.WHITE  
CAPSULE_SIZE = GRID_SIZE * 0.3 

SCARED_COLOR = color.WHITE    
SCARED_TIME = 180            

WARNING_TIME = 50


class Immovable:
    def is_a_wall(self):
        return False           

    def eat(self, chomp):    
        pass              


class Nothing(Immovable):
    pass


class Movable:
    def __init__(self, maze, point, speed):
        self.maze = maze 
        self.place = point 
        self.speed = speed 
        self.start = point 

    def furthest_move(self, movement):
        (move_x, move_y) = movement   
        (current_x, current_y) = self.place 
        nearest = self.nearest_grid_point() 
        (nearest_x, nearest_y) = nearest
        maze = self.maze

        if move_x > 0:            
            next_point = (nearest_x+1, nearest_y)
            if maze.object_at(next_point).is_a_wall():
                if current_x+move_x > nearest_x:  
                    move_x = nearest_x - current_x 

        elif move_x < 0:           
            next_point = (nearest_x-1, nearest_y)
            if maze.object_at(next_point).is_a_wall():
                if current_x+move_x < nearest_x:         
                    move_x = nearest_x - current_x       

        if move_y > 0:              
            next_point = (nearest_x, nearest_y+1)
            if maze.object_at(next_point).is_a_wall():
                if current_y+move_y > nearest_y:    
                    move_y = nearest_y - current_y     

        elif move_y < 0:           
            next_point = (nearest_x, nearest_y-1)
            if maze.object_at(next_point).is_a_wall():
                if current_y+move_y < nearest_y:  
                    move_y = nearest_y - current_y      

        if move_x > self.speed:     
            move_x = self.speed
        elif move_x < -self.speed:
            move_x = -self.speed

        if move_y > self.speed:
            move_y = self.speed
        elif move_y < -self.speed:
            move_y = -self.speed

        return (move_x, move_y)

    def nearest_grid_point(self):
        (current_x, current_y) = self.place
        grid_x = int(current_x + 0.5)      
        grid_y = int(current_y + 0.5)      
        return (grid_x, grid_y)           

    def capsule_eaten(self):        
        pass                       


class Chomp(Movable):
    def __init__(self, maze, point):
        self.direction = 0                  
        Movable.__init__(self, maze, point,  
                         CHOMP_SPEED)

    def move(self):
        keys = keys_pressed()
        if 'left' in keys:
            self.move_left()     
        elif 'right' in keys:
            self.move_right()   
        elif 'up' in keys:
            self.move_up()     
        elif 'down' in keys:
            self.move_down() 
        elif 'a' in keys:
            self.move_left()
        elif 'd' in keys:
            self.move_right()
        elif 'w' in keys:
            self.move_up()
        elif 's' in keys:
            self.move_down()
        self.maze.chomp_is(self, self.place)  

    def move_left(self):
        self.try_move((-1, 0))

    def move_right(self):
        self.try_move((1, 0))

    def move_up(self):
        self.try_move((0, 1))

    def move_down(self):
        self.try_move((0, -1))

    def try_move(self, move):
        (move_x, move_y) = move
        (current_x, current_y) = self.place
        (nearest_x, nearest_y) = (self.nearest_grid_point())

        
        if self.furthest_move(move) == (0, 0):
            return
       
        if move_x != 0 and current_y != nearest_y:
            move_x = 0                    
            move_y = nearest_y - current_y 
      
        elif move_y != 0 and current_x != nearest_x:
            move_y = 0                   
            move_x = nearest_x - current_x 

        move = self.furthest_move((move_x, move_y)) 
        self.move_by(move)

    def draw(self):
        maze = self.maze
        screen_point = maze.to_screen(self.place)
        
        angle = self.get_angle()
       
        endpoints = (self.direction + angle, self.direction + 360 - angle)
        self.body = Arc(
            screen_point,
            CHOMP_SIZE,  
            endpoints[0],
            endpoints[1],
            filled=True,
            color=CHOMP_COLOR
        )

    def get_angle(self):
        
        (x, y) = self.place
       
        (nearest_x, nearest_y) = (self.nearest_grid_point())
     
        distance = (abs(x-nearest_x) + abs(y-nearest_y))
     
        return 1 + 90 * distance

    def move_by(self, move):
        self.update_position(move)
        old_body = self.body    
        self.draw()           
        remove_from_screen(old_body) 

        (x, y) = self.place         
        nearest_point = self.nearest_grid_point()         
        (nearest_x, nearest_y) = nearest_point
        distance = (abs(x-nearest_x) +      
                    abs(y-nearest_y))

        if distance < self.speed * 3 / 4:   
            object = self.maze.object_at(nearest_point)
            object.eat(self)              

    def update_position(self, move):
        (old_x, old_y) = self.place     
        (move_x, move_y) = move        
        (new_x, new_y) = (old_x+move_x, old_y+move_y) 
        self.place = (new_x, new_y)               

        if move_x > 0:                  
            self.direction = 0         
        elif move_y > 0:             
            self.direction = 90     
        elif move_x < 0:               
            self.direction = 180      
        elif move_y < 0:          
            self.direction = 270     

    def chomp_is(self, chomp, point):
        pass                        


class Maze:
    def __init__(self, levels):
        self.levels = levels
        self.level = -1
        self.have_window = False   
        self.game_over = False   
        self.loss = False
        self.height = len(levels[0])
        self.width = len(levels[0][0])
        self.make_window(self.width, self.height)
        set_speed(40)           

    def next_level(self):
        self.level += 1
        if self.level >= len(self.levels) or self.loss:
            return False
        self.game_over = False
        clear_screen()
        self.set_layout(self.levels[self.level])   
        return True

    def set_layout(self, layout):
        self.make_map(self.width, self.height)   
        self.movables = []
        self.food_count = 0                    

        max_y = self.height - 1
        for x in range(self.width):          
            for y in range(self.height):
                char = layout[max_y - y][x] 
                self.make_object((x, y), char)  

        for movable in self.movables:          
            movable.draw()

    def make_window(self, width, height):
        grid_width = (width - 1) * GRID_SIZE  
        grid_height = (height - 1) * GRID_SIZE
        screen_width = 2 * MARGIN + grid_width
        screen_height = 2 * MARGIN + grid_height
       
        begin_graphics(screen_width, screen_height, "Chomp", BACKGROUND_COLOR)

    def to_screen(self, point):
        (x, y) = point
        x = x * GRID_SIZE + MARGIN  
        y = y * GRID_SIZE + MARGIN 
        return (x, y)

    def make_map(self, width, height):
        self.width = width               
        self.height = height
        self.map = []      
        new_row = []                       
        for x in range(width):
            new_row.append(Nothing())     
        for y in range(height):
            self.map.append(new_row[:])  

    def make_object(self, point, character):
        (x, y) = point
        if character == '%':           
            self.map[y][x] = Wall(self, point)
        elif character == 'P':         
            self.movables.append(Chomp(self, point))
        elif character == '.':
            self.food_count = self.food_count + 1   
            self.map[y][x] = Food(self, point)    
        elif character == 'G':             
            
            self.movables.append(Ghost(self, point))
        elif character == 'o':                  
            self.map[y][x] = Capsule(self, point)    

    def finished(self):
        return self.game_over        

    def done(self):
        end_graphics()             
        self.map = []
        self.movables = []

    def object_at(self, point):
        (x, y) = point

        if y < 0 or y >= self.height:    
            return Nothing()            

        if x < 0 or x >= self.width:
            return Nothing()

        return self.map[y][x]

    def play(self):
        for movable in self.movables:  
            movable.move()
        update_when('next_tick')      

    def remove_food(self, place):
        (x, y) = place
        self.map[y][x] = Nothing()   
        self.food_count = self.food_count - 1 
        if self.food_count == 0:             
            self.win()                      

    def win(self):
        print("And that is a win for you.")
        self.game_over = True

    def chomp_is(self, chomp, point):
        for movable in self.movables:     
            movable.chomp_is(chomp, point) 

    def lose(self):
        print("Oh crap! you lose.")
        self.game_over = True
        self.loss = True

    def remove_capsule(self, place):
        (x, y) = place
        self.map[y][x] = Nothing()      
        for movable in self.movables:  
            movable.capsule_eaten()  


class Wall(Immovable):
    def __init__(self, maze, point):
        self.place = point         
        self.screen_point = maze.to_screen(point)
        self.maze = maze          
        self.draw()

    def draw(self):
        (x, y) = self.place
        neighbors = [(x+1, y), (x-1, y),      
                     (x, y+1), (x, y-1)]
        for neighbor in neighbors:          
            self.check_neighbor(neighbor)

    def is_a_wall(self):
        return True                    

    def check_neighbor(self, neighbor):
        maze = self.maze
        object = maze.object_at(neighbor)      

        if object.is_a_wall():            
            here = self.screen_point    
            there = maze.to_screen(neighbor)      
            Line(here, there, color=WALL_COLOR, thickness=2)


class Food(Immovable):
    def __init__(self, maze, point):
        self.place = point
        self.screen_point = maze.to_screen(point)
        self.maze = maze
        self.draw()

    def draw(self):
        self.dot = Circle(self.screen_point,
                          FOOD_SIZE,
                          color=FOOD_COLOR,
                          filled=True)

    def eat(self, chomp):
        remove_from_screen(self.dot)         
        self.maze.remove_food(self.place)   


class Ghost(Movable):
    num = 0

    def __init__(self, maze, start):
        Ghost.num += 1
        self.next_point = start         
        self.movement = (0, 0)         

        self.color = GHOST_COLORS[Ghost.num % 4]  

        self.original_color = self.color     
        self.time_left = 0                 

        Movable.__init__(self, maze,    
                         start, GHOST_SPEED)

    def draw(self):
        maze = self.maze
        (screen_x, screen_y) = (
            maze.to_screen(self.place))
        coords = []                  
        for (x, y) in GHOST_SHAPE:
            coords.append((x*GRID_SIZE + screen_x,
                           y*GRID_SIZE + screen_y))

        self.body = Polygon(coords, color=self.color, 
                            filled=True)

    def move(self):
        (current_x, current_y) = self.place  
        (next_x, next_y) = self.next_point
        move = (next_x - current_x,
                next_y - current_y)
        move = self.furthest_move(move)     
        if move == (0, 0):                
            move = self.choose_move()   
        self.move_by(move)                  
        if self.time_left > 0:            
            self.update_scared()         

    def update_scared(self):
        self.time_left = self.time_left - 1 
        time_left = self.time_left
        if time_left < WARNING_TIME:      
            if time_left % 2 == 0:       
                color = self.original_color 
            else:
                color = SCARED_COLOR       
            self.change_color(color)      

    def choose_move(self):
        (move_x, move_y) = self.movement    
        (nearest_x, nearest_y) = (self.nearest_grid_point())
        possible_moves = []

        if move_x >= 0 and self.can_move_by((1, 0)): 
            possible_moves.append((1, 0))

        if move_x <= 0 and self.can_move_by((-1, 0)):  
            possible_moves.append((-1, 0))

        if move_y >= 0 and self.can_move_by((0, 1)):  
            possible_moves.append((0, 1))

        if move_y <= 0 and self.can_move_by((0, -1)):
            possible_moves.append((0, -1))

        
        if len(possible_moves) != 0:
           
            choice = random_between(0, len(possible_moves) - 1)
            move = possible_moves[choice]
            (move_x, move_y) = move
        else:
            move_x = -move_x          
            move_y = -move_y
            move = (move_x, move_y)

        (x, y) = self.place
        self.next_point = (x+move_x, y+move_y)  

        self.movement = move                   
        return self.furthest_move(move)      

    def can_move_by(self, move):
       
        move = self.furthest_move(move)
        return move != (0, 0)           

    def move_by(self, move):
        (old_x, old_y) = self.place       
        (move_x, move_y) = move             

        (new_x, new_y) = (old_x+move_x, old_y+move_y) 
        self.place = (new_x, new_y)            

        screen_move = (move_x * GRID_SIZE,
                       move_y * GRID_SIZE)
      
        move_by(self.body, screen_move[0], screen_move[1])

    def chomp_is(self, chomp, point):
        (my_x, my_y) = self.place
        (his_x, his_y) = point
        X = my_x - his_x
        Y = my_y - his_y
        DxD = X*X + Y*Y
        limit = 1.6*1.6
        if DxD < limit:
            self.bump_into(chomp)

    def capsule_eaten(self):
        self.change_color(SCARED_COLOR)  
        self.time_left = SCARED_TIME

    def change_color(self, new_color):
        self.color = new_color            
        self.redraw()               

    def redraw(self):
        old_body = self.body
        self.draw()
        remove_from_screen(old_body)

    def bump_into(self, chomp):       
        if self.time_left != 0:   
            self.captured(chomp)    
        else:
            self.maze.lose()         

    def captured(self, chomp):
        self.place = self.start            
        self.color = self.original_color    
        self.time_left = 0                
        self.redraw()                       


class Capsule(Immovable):
    def __init__(self, maze, point):
        self.place = point
        self.screen_point = maze.to_screen(point)
        self.maze = maze
        self.draw()

    def draw(self):
        (screen_x, screen_y) = self.screen_point
        self.dot = Circle((screen_x, screen_y),
                          CAPSULE_SIZE,
                          color=CAPSULE_COLOR,
                          filled=True)

    def eat(self, chomp):
        remove_from_screen(self.dot)         
        self.maze.remove_capsule(self.place)  


def create_levels(file_name):
    levels = []
    level = []
    making_level = False

    file = open(file_name, "r")

    for line in file.readlines():
        line = line[:-1]

        if "[start maze]" in line:
            level = []
            making_level = True
            first = True
            len_first = 0

        elif "[end maze]" in line:
            levels.append(level)
            making_level = False

        elif first:
            first = False
            len_first = len(line)

        elif making_level and len(line) != len_first:
            print(line)
            raise ValueError("Line Lengths don't match!")

        if making_level and "[start maze]" not in line:
            level_line = ""
            for character in line:
                if character in "%.oGP ":
                    level_line += character
            level.append(level_line)

    return levels



the_maze = Maze(create_levels("chomp_levels.txt"))

while the_maze.next_level():
    while not the_maze.finished():   
        the_maze.play()

the_maze.done()       

